;;; sql-duckdb.el --- DuckDB support for sql.el -*- lexical-binding: t -*-

;; Copyright (C) 2020-2020 Ahmadou Dicko <mail@ahmadoudicko.com>

;; Author: Ahmadou Dicko <mail@ahmadoudicko.com>
;; Version: 1.0
;; Keywords: sql, duckdb
;; URL: https://gitlab.com/dickoa/sql-duckdb

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License along with
;; this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package adds comint support for the DuckDB command-line interface.
;; It depends on an installed and functional DuckDB command-line interface.

(require 'sql)

(defvar sql-mode-duckdb-font-lock-keywords
  (eval-when-compile
    (list
     ;; DuckDB commands
     '("^[.].*$" . font-lock-doc-face)

     ;; DuckDB Keyword
     (sql-font-lock-keywords-builder 'font-lock-keyword-face nil
				     "abort" "action" "add" "after" "all" "alter" "analyze" "and" "as"
				     "asc" "attach" "autoincrement" "before" "begin" "between" "by"
				     "cascade" "case" "cast" "check" "collate" "column" "commit" "conflict"
				     "constraint" "create" "cross" "database" "default" "deferrable"
				     "deferred" "delete" "desc" "describe" "detach" "distinct" "drop" "each" "else"
				     "end" "escape" "except" "exclusive" "exists" "explain" "fail" "for"
				     "foreign" "from" "full" "glob" "group" "having" "if" "ignore"
				     "immediate" "in" "index" "indexed" "initially" "inner" "insert"
				     "instead" "intersect" "into" "is" "isnull" "join" "key" "left" "like"
				     "limit" "match" "natural" "no" "not" "notnull" "null" "of" "offset"
				     "on" "or" "order" "outer" "plan" "pragma" "primary" "query" "raise"
				     "references" "regexp" "reindex" "release" "rename" "replace"
				     "restrict" "right" "rollback" "row" "savepoint" "select" "set" "table"
				     "temp" "temporary" "then" "to" "transaction" "trigger" "union"
				     "unique" "update" "using" "vacuum" "values" "view" "virtual" "when"
				     "where")

     ;; DuckDB Data types
     (sql-font-lock-keywords-builder 'font-lock-type-face nil
				     "int" "integer" "tinyint" "smallint" "mediumint"
				     "bigint" "unsigned" "big" "int2" "int8" "int4" "character"
				     "varchar" "varying" "nchar" "native" "nvarchar" "text"
				     "bpchar" "clob" "blob" "real" "double" "float4" "precision" "float"
				     "bool" "boolean" "numeric" "number" "decimal"
				     "boolean" "date" "datetime" "timestamp")

     ;; DuckDB Functions
     (sql-font-lock-keywords-builder 'font-lock-builtin-face nil
				     ;; Numeric functions
  				     "abs" "acos" "atan2" "bit_count" "cbrt" "ceil" "cos"
				     "cot" "degrees" "floor" "ln" "log" "log2" "pi" "pow"
				     "radians" "random" "round" "setseed" "sin" "sign"
				     "sqrt" "tan"
				     ;; Text functions
				     "string" "concat" "concat_ws" "format" "left"
				     "length" "lower" "lpad" "ltrim" "printf" "upper"
				     "regexp_replace" "regexp_full_match" "replace"
				     "repeat" "reverse" "right" "rpad" "rtrim" "strlen"
				     "strip_accents" "substring" "trim" "unicode"
				     ;; Date functions
				     "current_date" "date_trunc" "extract" "strftime"
				     "strptime"
				     ;; Timestamp functions
				     "current_timestamp" "age" "current_date" "current_time"
				     "epoch_ms" "now"
				     ;; Blob functions
				     "octet_length"
				     ;; Other functions
				     "read_csv" "read_csv_auto"
				     ;; Aggregate functions
				     "avg" "count" "group_concat" "string_agg" "max" "min" "sum" "total"
				     "bit_or" "bit_xor" "stddev_samp" "stddev_pop" "var_samp"
				     "var_pop"))))


(defcustom sql-duckdb-program "duckdb"
  "Command to start DuckDB.

  Starts `sql-interactive-mode' after doing some setup."
  :type 'file
  :group 'SQL)

(defcustom sql-duckdb-options nil
  "List of additional options for `sql-duckdb-program'."
  :type '(repeat string)
  :group 'SQL)

(defcustom sql-duckdb-login-params '((database :file nil
                                               :must-match confirm))
  "List of login parameters needed to connect to DuckDB."
  :type 'sql-login-params
  :group 'SQL)


(defun sql-comint-duckdb (product options &optional buf-name)
  "Create comint buffer and connect to DuckDB."
  ;; Put all parameters to the program (if defined) in a list and call
  ;; make-comint.
  (let ((params
         (append options
                 (if (not (string= "" sql-database))
                     `(,(expand-file-name sql-database))))))
    (sql-comint product params buf-name)))


;;;###autoload
(defun sql-duckdb (&optional buffer)
  "Run DuckDB as an inferior process.

  DuckDB is free software.

  If buffer `*SQL*' exists but no process is running, make a new process.
  If buffer exists and a process is running, just switch to buffer
  `*SQL*'.

  Interpreter used comes from variable `sql-duckdb-program'.  Login uses
  the variables `sql-user', `sql-password', `sql-database', and
  `sql-server' as defaults, if set.  Additional command line parameters
  can be stored in the list `sql-duckdb-options'.

  The buffer is put in SQL interactive mode, giving commands for sending
  input.  See `sql-interactive-mode'.

  To set the buffer name directly, use \\[universal-argument]
  before \\[sql-duckdb].  Once session has started,
  \\[sql-rename-buffer] can be called separately to rename the
  buffer.

  To specify a coding system for converting non-ASCII characters
  in the input and output to the process, use \\[universal-coding-system-argument]
  before \\[sql-duckdb].  You can also specify this with \\[set-buffer-process-coding-system]
  in the SQL buffer, after you start the process.
  The default comes from `process-coding-system-alist' and
  `default-process-coding-system'.

  \(Type \\[describe-mode] in the SQL buffer for a list of commands.)"
  (interactive "P")
  (sql-product-interactive 'duckdb buffer))

(sql-add-product
 'duckdb "DuckDB"
 :free-software t
 :font-lock sql-mode-duckdb-font-lock-keywords
 :sqli-program 'sql-duckdb-program
 :sqli-options 'sql-duckdb-options
 :sqli-login 'sql-duckdb-login-params
 :sqli-comint-func 'sql-comint-duckdb
 :list-all ".tables"
 :list-table ".schema %s"
 :prompt-regexp "^D "
 :prompt-length 3
 :prompt-cont-regexp "^   \\.\\.\\.> ")

(provide 'sql-duckdb)
;;; End of sql-duckdb.el
